# Generated by go2rpm 1.3
%bcond_without check

# https://github.com/emersion/go-imap
%global goipath         github.com/emersion/go-imap
Version:                1.0.6

%gometa

%global common_description %{expand:
An IMAP library for clients and servers.}

%global golicenses      LICENSE
%global godocs          README.md

Name:           %{goname}
Release:        1%{?dist}
Summary:        An IMAP library for clients and servers

License:        MIT
URL:            %{gourl}
Source0:        %{gosource}

BuildRequires:  golang(github.com/emersion/go-message)
BuildRequires:  golang(github.com/emersion/go-message/mail)
BuildRequires:  golang(github.com/emersion/go-message/textproto)
BuildRequires:  golang(github.com/emersion/go-sasl)
BuildRequires:  golang(golang.org/x/text/encoding)
BuildRequires:  golang(golang.org/x/text/transform)

%description
%{common_description}

%gopkg

%prep
%goprep

%install
%gopkginstall

%if %{with check}
%check
%gocheck
%endif

%gopkgfiles

%changelog
* Mon Dec 28 15:23:53 CST 2020 proletarius101 <proletarius101@protonmail.com> - 1.0.6-1
- Initial package

