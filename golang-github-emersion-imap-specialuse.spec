# Generated by go2rpm 1.3
%bcond_without check

# https://github.com/emersion/go-imap-specialuse
%global goipath         github.com/emersion/go-imap-specialuse
%global commit          1ab93d3d150e1fa13e4c12cadbe04e0a976f5c5b

%gometa

%global common_description %{expand:
Special-Use Mailboxes extension for go-imap.}

%global golicenses      LICENSE
%global godocs          README.md

Name:           %{goname}
Version:        0
Release:        0.1%{?dist}
Summary:        Special-Use Mailboxes extension for go-imap

License:        MIT
URL:            %{gourl}
Source0:        %{gosource}

BuildRequires:  golang(github.com/emersion/go-imap/server)

%description
%{common_description}

%gopkg

%prep
%goprep

%install
%gopkginstall

%if %{with check}
%check
%gocheck
%endif

%gopkgfiles

%changelog
* Mon Dec 28 15:23:53 CST 2020 proletarius101 <proletarius101@protonmail.com> - 0-0.1.20201228git1ab93d3
- Initial package

