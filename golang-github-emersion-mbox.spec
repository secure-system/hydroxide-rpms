# Generated by go2rpm 1.3
%bcond_without check

# https://github.com/emersion/go-mbox
%global goipath         github.com/emersion/go-mbox
Version:                1.0.2

%gometa

%global common_description %{expand:
Package mbox parses the mbox file format into messages and formats messages
into mbox files.}

%global golicenses      LICENSE
%global godocs          README.md

Name:           %{goname}
Release:        1%{?dist}
Summary:        Mbox file parser

License:        MIT
URL:            %{gourl}
Source0:        %{gosource}

%description
%{common_description}

%gopkg

%prep
%goprep

%install
%gopkginstall

%if %{with check}
%check
%gocheck
%endif

%gopkgfiles

%changelog
* Mon Dec 28 15:23:53 CST 2020 proletarius101 <proletarius101@protonmail.com> - 1.0.2-1
- Initial package

