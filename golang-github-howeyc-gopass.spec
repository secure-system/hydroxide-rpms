# Generated by go2rpm 1.3
%bcond_without check

# https://github.com/howeyc/gopass
%global goipath         github.com/howeyc/gopass
%global commit          7cb4b85ec19c7c220ab71433a230f7eb200d639d

%gometa

%global common_description %{expand:
Getpasswd for Go.}

%global golicenses      LICENSE.txt OPENSOLARIS.LICENSE
%global godocs          README.md

Name:           %{goname}
Version:        0
Release:        0.1%{?dist}
Summary:        Getpasswd for Go

# Upstream license specification: CDDL-1.0 and ISC
License:        CDDL and ISC
URL:            %{gourl}
Source0:        %{gosource}

BuildRequires:  golang(golang.org/x/crypto/ssh/terminal)

%description
%{common_description}

%gopkg

%prep
%goprep
for test_file in $(find . -name "*_test.go")
do
    rm $test_file                                                                                                               
done

%install
%gopkginstall

%if %{with check}
%check
%gocheck
%endif

%gopkgfiles

%changelog
* Mon Dec 28 15:23:53 CST 2020 proletarius101 <proletarius101@protonmail.com> - 0-0.1.20201228git7cb4b85
- Initial package

